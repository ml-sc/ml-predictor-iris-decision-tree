import pathlib
from copy import deepcopy

import joblib
import numpy as np

clf = joblib.load(pathlib.Path(__file__).parent / 'data' / 'model.pkl')


def predict(data: dict):
    result = deepcopy(data)
    try:
        values = data.get('data', {}).get('values')
        if values:
            p = clf.predict(np.array(values).reshape(1, -1))
            result.update({'result': {'prediction': str(p)}})
        else:
            result.update({'result': {'prediction': 'no data'}})
    except:
        result.update({'result': {'prediction': 'invalid data'}})

    return result
