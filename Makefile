.PHONY: clean test run deploy destroy
NAME_FILE:=src/data/name
NAME:=$(shell cat ${NAME_FILE})

venv:
	python3.7 -m venv venv

install: venv
	venv/bin/pip install -r src/requirements.txt
	venv/bin/pip install -r tests/requirements.txt

test: install
	@echo "Testing: $(NAME)"
	PYTHONPATH="src:tests" venv/bin/python -m pytest tests/ -v

clean:
	rm -rf venv .pytest_cache

run:
	bash -c "trap 'docker-compose down' EXIT; docker-compose up --build"

deploy:
	NAME=$(NAME) docker-compose -p $(NAME) up --build -d

destroy:
	NAME=$(NAME) docker-compose -p $(NAME) down